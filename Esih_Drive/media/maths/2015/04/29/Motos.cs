﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domaine
{
    class Motos
    {
        //Attributs
        private int nif;
        private int iDMoto;
        private int noBlouson;
        private int idFiscalite;
        private string marque;
        private string dateAjout;
        private string exerciceFiscale;
        private string plaque;



        // propietés
        public int Nif
        {
            get { return nif; }
            set
            {
                if (nif <= 0)
                {
                    nif = value;
                }
                else
                {
                    //On genere une exception
                    throw new Exception("ce champ ne peut etre vide");
                }
            }

        }
        public int IdMoto
        {
            get { return iDMoto; }
            set
            {
                if (iDMoto <= 0)
                {
                    iDMoto = value;
                }
                else
                {
                    //On genere une exception
                    throw new Exception("ce champ ne peut etre vide");
                }
            }

        }

         public int NoBlouson
        {
            get { return noBlouson; }
            set
            {
                if (noBlouson <= 0)
                {
                    noBlouson = value;
                }
                else
                {
                    //On genere une exception
                    throw new Exception("ce champ ne peut etre vide");
                }
            }

        }
        
        public int IdFiscalite
        {
            get { return idFiscalite; }
            set
            {
                if (idFiscalite <= 0)
                {
                    idFiscalite = value;
                }
                else
                {
                    //On genere une exception
                    throw new Exception("ce champ ne peut etre vide");
                }
            }

        }

        public string Marque
        {
            get { return marque; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    marque = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }

        public string DateAjout
        {
            get { return dateAjout; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    dateAjout = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }

        public string ExerciceFiscale
        {
            get { return exerciceFiscale; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    exerciceFiscale = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }



        public string Plaque
        {
            get { return plaque; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    plaque = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }




        //  contructeurs
        public Motos(int n, int d, string nm)
        {
            this.iDMoto = d;
            this.nif = n;
            this.marque = nm.Trim();
        }
        //constructeurs motos Chauffeurs
        public void MotoChauffeurs(int n, int d,int b, string nm)
        {   this.nif = n;
            this.iDMoto = d;
            this.noBlouson=b;
            this.dateAjout = nm.Trim();

        }

        //constructeurs Fiscalite Motos
        public void FiscalitesMotos(int n, int d, string ex, string p)
        {
            this.idFiscalite = n;
            this.iDMoto = d;
            this.exerciceFiscale = ex.Trim();
            this.dateAjout = p.Trim();

        }
        //Methode utiles
        public override string ToString()
        {
            //override c'est une redefinition de la superclasse motos
            return string.Format("{0} {1} {2} {3} {4} {5} {6} {7} ", nif, iDMoto, noBlouson, idFiscalite, marque, dateAjout, exerciceFiscale, plaque);
        }
             
    }
}
