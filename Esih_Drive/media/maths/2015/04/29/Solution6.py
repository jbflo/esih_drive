__author__ = 'jb-flo'

"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 6

"""

# Exercise VI.1. Python provides a data structure called set that provides many common set operations.
# Read the documentation at http://docs.python.org/2/library/stdtypes.html# types-set
# and write a program that uses set subtraction to ﬁnd words in the book that are not in the word list.


import string


def process_file(filename, skip_header):
    """
     histogramme and skip_header fuction

    """
    hist = {}
    fp = open(filename)

    if skip_header:
        skip_gutenberg_header(fp)

    for line in fp:
        process_line(line, hist)
    return hist


def skip_gutenberg_header(fp):
    """Reads from fp until it finds the line that ends the header.

    fp: open file object
    """
    for line in fp:
        if line.startswith('*** START OF THIS PROJECT'):
            break


def process_line(line, hist):
    """Adds the words in the line to the histogram.
    line: string
    hist: histogram (map from word to frequency)
    """
    # replace hyphens with spaces before splitting
    line = line.replace('-', ' ')

    for word in line.split():
        # remove punctuation and convert to lowercase
        word = word.strip(string.punctuation + string.whitespace)
        word = word.lower()

        # update the histogram
        hist[word] = hist.get(word, 0) + 1


def main():
    hist = process_file('multilinguisme.txt', skip_header=True)

    words = process_file('Dorian.txt', skip_header=False)

    word_find = set(hist) - set(words)
    print("The words in the book that aren't in the word list are:")
    for word in word_find:
        print(word)

main()
