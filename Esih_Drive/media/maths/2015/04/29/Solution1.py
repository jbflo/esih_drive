__author__ = 'jb-flo'

"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 1

"""

# Exercise I.1. Write a program that reads a ﬁle, breaks each line into words,
# strips white space and punctuation from the words, and converts them to lowercase.

import os
import string

def words():
    data = open('word.txt', 'r')
    for w in data:
        for word in w.split():
            word = word.strip(string.punctuation + string.whitespace)
            word = word.lower()
            print(word)
words()
os.system("pause")