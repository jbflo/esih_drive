__author__ = 'jb-flo'

"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 2
"""

# Exercise I.3. Modify the program from the previous exercise to print the 20 most frequently-used words in the book.

import string
import os


def words():
    flag = False
    signal = "*** START OF"
    data = open('multilinguisme.txt', 'r')
    wo = []
    for w in data:
        if flag == True:
            for word in w.split():
                word = word.strip(string.punctuation + string.whitespace)
                word = word.lower()
                wo.append(word)
        elif (signal in w) and (flag == False):
            flag = True
        else:
            pass
    data.close()
    return wo

def histogram(data):
    hist = {}
    for word in data:
        if word == '':
            pass
        else:
            hist[word] = hist.get(word, 0) + 1
    return hist


def main():

        data = words()
        print("Stats for multilinguisme.txt:")
        hist = histogram(data)
        top20 = []
        for key in hist:
            top20.append([hist[key], key])
        top20.sort(reverse=True)
        for i in range(0, 20):
            print("  {}) {} {}".format(i + 1, top20[i][1], top20[i][0]))

        print("\n")

main()

os.system("pause")