__author__ = 'jb-flo'

"""
Created on Tue Jan 20 17:54:42 2015
      by Florial Jean Baptiste
             ESIH
      Master 1 data base
        Solution Exo 5
"""

# Exercise II.1. Write a function named choose_from_hist  that takes a histogram
# as deﬁned in and returns a random value from the histogram, chosen with probability in proportion
# to frequency (you will have to create a histogram first).


import random
import os


def choose_from_hist():
    t = ['a', 'a', 'b']
    hist = {}
    for item in t:
        hist[item] = hist.get(item, 0) + 1

    list_ = []
    for key in hist:
        for i in range(0, hist[key]):
            list_.append(key)
    return random.choice(list_)


def stats():
    a = 0
    b = 0
    for i in range(0, 100):
        if choose_from_hist() == 'a':
            a += 1
        else:
            b += 1
    print("a: {}.5f".format(a / 100.0),'\n',"b:{}.5f".format(b / 100.0))

stats()

os.system("pause")