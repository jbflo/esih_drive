﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domaine
{
    public class Individus
    {
        //Attributs
        private int nif;
        private int dossier;

        private string nom;
        private string prenom;
        private string sexe;
        private string type;



        // propietés
        public int Nif
        {
            get { return nif; }
            set
            {
                if (nif <= 0)
                {
                    nif = value;
                }
                else
                {
                    //On genere une exception
                    throw new Exception("ce champ ne peut etre vide");
                }
            }

        }
        public int Dossier
        {
            get { return dossier; }
            set
            {
                if (dossier <= 0)
                {
                    dossier = value;
                }
                else
                {
                    //On genere une exception
                    throw new Exception("ce champ ne peut etre vide");
                }
            }

        }

        public string Nom
        {
            get { return nom; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    nom = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }
        public string Prenom
        {
            get { return prenom; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    prenom = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }
        public string Sexe
        {
            get { return sexe; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    sexe = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }



        public string Type
        {
            get { return type; }
            set
            {
                if (!String.IsNullOrEmpty(value.Trim()))
                {
                    type = value;
                }
                else
                {
                    throw new Exception("ce champ ne peut pas etre vide");
                }
            }
        }




        // Les contructeurs
        public Individus(int n, int d, string nm, string prnm, string se, string tp)
        {
            this.nif = n;
            this.dossier = d;

            this.nom = nm.Trim();
            this.prenom = prnm.Trim();
            this.sexe = se.Trim();
            this.type = tp.Trim();
        }

        //Methode utiles
        public override string ToString()
        {
            //override c'est une redefinition dde la superclasse personne
            return string.Format("{0} {1} {2} {3} {4} {5}", nif, dossier, nom, prenom, sexe, type);
        }
             
        
    }
}
