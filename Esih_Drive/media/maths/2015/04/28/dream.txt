Indiquez le principal objectif de votre candidature.

Le programme de mobilit� propos� doit d�crire le lien 
existant entres les activit�s que vous avez d�velopp�es 
dans votre institution d�origine et ce que vous allez 
d�velopper dans l'�tablissement d'accueil. Ce programme 
de mobilit� sera utilis� par l'universit� d'accueil afin 
d'�valuer la pertinence et l'int�r�t de cette mise en �uvre. 
La proposition de mobilit� finale sera, en cas de s�lection, 
articul�e et d�finie par le boursier, le coordinateur et 
l'universit� d'accueil et, dans un cas sp�cifique, l'�tablissement
d'origine.


Pourquoi avez-vous choisi cette institution? *
(1000 caract�res m�x.)

D�crivez bri�vement l'impact de votre programme de mobilit� sur le d�veloppement de votre activit� professionnelle. *
(1000 caract�res m�x.)

Expliquez bri�vement comment votre programme de mobilit� pourrait-il contribuer au d�veloppement des relations institutionnelles entre votre institution d�origine et les institutions d�accueil. *
(1000 caract�res m�x.)

D�crivez bri�vement quel impact au niveau de la r�gion/pays d�origine, attendez-vous � partir de votre participation dans ce projet. *
(1000 caract�res m�x.)
