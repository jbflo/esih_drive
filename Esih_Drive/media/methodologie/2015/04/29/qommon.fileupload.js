$(function() {
    $('.file-upload-widget').each(function() {
        var base_widget = $(this);
        if ($(base_widget).find('input[type=hidden]').val()) {
            $(base_widget).find('input[type=file]').hide();
        } else {
            $(base_widget).find('.fileinfo').hide();
        }
        $(this).find('input[type=file]').fileupload({
            dataType: 'json',
            add: function (e, data) {
                $(base_widget).find('.fileprogress .bar').css('width', '0%');
                $(base_widget).find('.fileprogress').show();
                $(base_widget).find('.fileinfo').hide();
                var jqXHR = data.submit();
            },
            done: function(e, data) {
                $(base_widget).find('.fileprogress').hide();
                $(base_widget).find('.filename').text(data.result[0].name);
                $(base_widget).find('.fileinfo').show();
                $(base_widget).find('input[type=hidden]').val(data.result[0].token);
                $(this).hide();
            },
            progress: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $(base_widget).find('.fileprogress .bar').css('width', progress + '%');
            }
        });
        $(this).find('a.remove').click(function() {
            $(base_widget).find('input[type=hidden]').val('');
            $(base_widget).find('.fileinfo').hide();
            $(base_widget).find('input[type=file]').show();
            return false;
        });
        $(this).find('a.change').click(function() {
            $(base_widget).find('input[type=file]').click();
            return false;
        });
    });
});
