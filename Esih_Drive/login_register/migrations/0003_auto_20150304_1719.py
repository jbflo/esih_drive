# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('login_register', '0002_register'),
    ]

    operations = [
        migrations.CreateModel(
            name='RegisterProf',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=70, unique=True)),
                ('sex', models.CharField(max_length=2, default=1, choices=[('1', 'Male'), ('2', 'Femal')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='RegisterStudent',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=50)),
                ('email', models.EmailField(max_length=70, unique=True)),
                ('level', models.CharField(max_length=2, default=1, choices=[('1', 'Prop'), ('2', 'l1 info'), ('3', 'l2 info'), ('4', 'l3 info'), ('5', 'M1 info'), ('6', 'M2 Mone'), ('7', 'l1 economie & gesttion'), ('8', 'l2 economie & gesttion'), ('9', 'l3 economie & gesttion'), ('10', 'l3 Scs Comptable'), ('11', 'M1 Scs Comptable')])),
                ('sex', models.CharField(max_length=2, default=1, choices=[('1', 'Male'), ('2', 'Femal')])),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='register',
            name='user',
        ),
        migrations.DeleteModel(
            name='Register',
        ),
    ]
