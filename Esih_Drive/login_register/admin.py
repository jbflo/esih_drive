from django.contrib import admin
from login_register.models import UserProfile
from login_register.models import RegisterStudent
from login_register.models import RegisterProf
from login_register.models import User


admin.site.register(UserProfile)
admin.site.register(RegisterStudent)
admin.site.register(RegisterProf)
# admin.site.register(User)
# Register your models here.
