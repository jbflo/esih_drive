__author__ = 'jb-flo'

from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from login_register.models import UserProfile

# from login_register.models import User

#
class UserForm(UserCreationForm):
    username = forms.CharField(max_length=User._meta.get_field('username').max_length)
    email = forms.CharField(max_length=User._meta.get_field('email').max_length)
    password = forms.CharField(min_length=6, max_length=16, widget=forms.PasswordInput())

    class Meta:
        model = UserCreationForm
        fields = ('fullname', 'username', 'email', 'password')


# class RegisterEmailForm(UserForm):
#
#     fullname = forms.CharField(max_length=User._meta.get_field('first_name').max_length)
#
#     class Meta(UserForm.Meta):
#         fields = UserForm.Meta.fields + ('fullname',)


class UserProfileForm(forms.ModelForm):
    class Meta:
        model = UserProfile
        fields = ('picture',)
