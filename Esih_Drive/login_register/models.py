from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


# Create your models here.


def get_level_choices():
    choices_list = (
    ('1', 'Prop'),
    ('2', 'l1 info'),
    ('3', 'l2 info'),
    ('4', 'l3 info'),
    ('5', 'M1 info'),
    ('6', 'M2 Mone'),
    ('7', 'l1 economie & gesttion'),
    ('8', 'l2 economie & gesttion'),
    ('9', 'l3 economie & gesttion'),
    ('10', 'l3 Scs Comptable'),
    ('11', 'M1 Scs Comptable'), )
    return choices_list


def get_sex_choices():
    choices_list = (
    ('1', 'Male'),
    ('2', 'Femal'), )
    return choices_list




class RegisterStudent(models.Model):
        name = models.CharField(max_length=50)
        user = models.OneToOneField(User)
        email = models.EmailField(max_length=70, unique=True)
        level = models.CharField(max_length=2, choices=get_level_choices(), default=1)
        sex = models.CharField(max_length=2, choices=get_sex_choices(), default=1)

        def clean_email(self):
            if (self.cleaned_data.get('email', '').endswith('esih.edu')):

                raise ValidationError("Invalid email address.")

            return self.cleaned_data.get('email', '')


class RegisterProf(models.Model):

        name = models.CharField(max_length=50)
        user = models.OneToOneField(User)
        email = models.EmailField(max_length=70, unique=True)
        sex = models.CharField(max_length=2, choices=get_sex_choices(), default=1)


# class User(models.Model):
#     username = models.OneToOneField(User)
#     password = models.CharField(min_length=6, max_length=16)
#
#     class Meta:
#         model = User
#         fields = ('username', 'email', 'password')


class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    website = models.URLField(blank=True)
    picture = models.ImageField(upload_to='profile_images', blank=True)

    # Override the __unicode__() method to return out something meaningful!
    def __unicode__(self):
        return self.user.username