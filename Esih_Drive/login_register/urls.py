from django.conf.urls import patterns, url
from login_register import views

urlpatterns = patterns('',
        url(r'^$', views.index),
        # url(r'^register/$', views.register, name='register'),  # ADD NEW PATTERN!
        url(r'^login/$', views.user_login, name='login'),
        # url(r'^login$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}),
        # url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page': '/'}),
        url(r'^restricted/', views.restricted, name='restricted'),
        url(r'^logout/$', views.user_logout, name='logout'),
)