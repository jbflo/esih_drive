from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('drive.views',
    url(r'^form/', include('login_register.urls')),
    url(r'^',  include('drive.urls', namespace='drive')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^books_cbv/', include('books_cbv.urls', namespace='books_cbv')),
    url(r'^books_fbv/', include('books_fbv.urls', namespace='books_fbv')),

    # url(r'^prop/(?P<id>\d+)$', 'get_fichier_prop', name='prop'),


)

# if debug is not true on deployment mode
if not settings.DEBUG:
        urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

# if debug is true on deployment mode
if settings.DEBUG:
    urlpatterns += patterns(
        'django.views.static',
        (r'media/(?P<path>.*)', 'serve', {'document_root': settings.MEDIA_ROOT}), )