from django.shortcuts import render
from django.shortcuts import render_to_response

from django.conf import settings
from django.shortcuts import redirect
from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.core.urlresolvers import reverse_lazy

from drive.models import Math
from drive.forms import  DocumentMath

from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http import Http404

from drive.forms import CoursPropForm

from django.core.context_processors import csrf

from drive.models import CoursProp
from drive.models import FichiersCoursProp
from django.core.urlresolvers import reverse_lazy
from django.views.generic.edit import DeleteView

# Create your views here.


def index(request):
    if not request.user.is_authenticated():
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    else:
        return render(request, 'drive/index.html')

# /////////////////////////////////////////////// Views Prop ///////////////////////////////////////////////////////////

# abc change to  get_coursProp

def get_coursProp(request):
    add = CoursPropForm()
    cours_prop = CoursProp.objects.all()
    return render(request, 'drive/prop.html', locals())

# adcd change to get_fichierProp

def get_fichier_prop(request, id):
    try:
        cours = CoursProp.objects.all()
        cours_prop = CoursProp.objects.get(id=id)
        doc_Prop = FichiersCoursProp.objects.filter(cours_prop=id)
    except FichiersCoursProp.DoesNotExist:
        raise Http404
    if not request.user.is_authenticated():
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    else:
        return render(request, 'drive/prop.html', locals())


def add_cours_prop(request):
    registered = False
    if request.method == 'POST':
        form_prop = CoursPropForm(request.POST, request.FILES)
        if form_prop.is_valid():
            cours_prop = request.POST['cours_prop']
            doc_Prop = form_prop.cleaned_data['doc_Prop']
            form_prop.save()
            registered = True
            return render(request, 'drive/new.html', locals())
        else:
            print(form_prop.errors)
    else:
        form_prop = CoursPropForm()
        var = {'form_prop': form_prop}
        var.update(csrf(request))

    if not request.user.is_authenticated():
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    else:
        return render_to_response('drive/new.html', var)

class UpdateFile(UpdateView):
    template_name = 'drive//new.html'
    model = FichiersCoursProp
    success_url = reverse_lazy('/prop/1')

class DeleteFile (DeleteView):
    template_name = 'drive/delete.html'
    model = FichiersCoursProp
    success_url = '/get_coursProp/id'








# ////////////////////////////////////////////////  Old Views  ////////////////////////////////////////////////////////



class PropMath(ListView):
      template_name = 'drive/prop.html'
      model = Math

class PropCreateMath(CreateView):
    template_name = 'drive/new.html'
    model = Math
    success_url = reverse_lazy('drive:prop')

class PropUpdateMath(UpdateView):
    template_name = 'drive/new.html'
    model = Math
    success_url = reverse_lazy('drive:prop')


class PropDeleteMath(DeleteView):
    template_name = 'drive/delete.html'
    model = Math
    success_url = reverse_lazy('drive:prop')

# ////////////////////////////////////////////////////////////////////////////////////////////////


# def prop(request):
#     # Handle file upload
#     if request.method == 'POST':
#         form = DocumentMath(request.POST, request.FILES)
#         if form.is_valid():
#             newdoc_math = Math(doc_math=request.FILES['doc_math'])
#             newdoc_math.save()
#             # form.save()
#
#         form1 = DocumentMethodologie(request.POST, request.FILES)
#         if form1.is_valid():
#             newdoc_meth = Methodologie(doc_methodologie=request.FILES['doc_methodologie'])
#             newdoc_meth.save()
#             # form1.save()
#
#             # Redirect to the document list after POST
#         return HttpResponseRedirect(reverse('drive.views.prop'))
#     else:
#         form = DocumentMath()  # A empty, unbound form
#         form1 = DocumentMethodologie()
#
#     # Load documents for the list page
#     maths = Math.objects.all()
#     methodologie = Methodologie.objects.all()
#
#     # Render list page with the documents and the form
#     if not request.user.is_authenticated():
#         return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
#     else:
#         return render_to_response(
#             'drive/prop.html', locals(), context_instance = RequestContext(request)
#
#             )
#
# def delete_document(request, pk):
#     try:
#         Math.objects.get(pk=pk).delete()
#     except Math.DoesNotExist:
#         pass
#     return HttpResponseRedirect('drive.views.l1')
