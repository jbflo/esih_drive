from django.contrib import admin
from drive.models import CoursProp, FichiersCoursProp, Math

admin.site.register(FichiersCoursProp)
admin.site.register(CoursProp)
admin.site.register(Math)
