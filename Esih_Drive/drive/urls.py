__author__ = 'jb-flo'

from django.conf.urls import patterns, url
from drive import views
from drive.views import DeleteFile


urlpatterns = patterns('drive.views',
        url(r'^$', views.index, name='index'),

        url(r'^prop/(?P<id>\d+)$', 'get_fichier_prop', name='prop'),
        url(r'^new/$',  'add_cours_prop', name='new'),
        url(r'^edit/(?P<pk>\d+)$',   views.UpdateFile.as_view(), name='edit_file'),
        url(r'^delete/(?P<pk>\d+)$', DeleteFile.as_view(), name='delete_file'),
)
