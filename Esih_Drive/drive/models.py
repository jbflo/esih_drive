from django.db import models
from django.core.urlresolvers import reverse

# ///////////////////////////////////////////////////// Models CoursProp ///////////////////////////////////////////


class CoursProp(models.Model):
    prop_cours = models.CharField(max_length=42)

    def __unicode__(self):
        return self.prop_cours


class FichiersCoursProp(models.Model):
    cours_prop = models.ForeignKey(CoursProp)
    description_prop = models.CharField(max_length=100)
    doc_Prop = models.FileField(upload_to='DocumentProp/%Y/%m/%d')

    def __unicode__(self):
        return self.description

    def get_absolute_url(self):
        return reverse('drive:edit', kwargs={'pk': self.pk})




# /////////////////////////////////////////////////   Old Modelds ////////////////////////////////////////////////

class Math (models.Model):
    name = models.CharField(max_length=50)
    doc_math = models.FileField(upload_to='maths/%Y/%m/%d')

    def __unicode__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('drive:edit', kwargs={'pk': self.pk})

