# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drive', '0009_auto_20150429_1836'),
    ]

    operations = [
        migrations.CreateModel(
            name='CoursProp',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('prop_cours', models.CharField(max_length=42)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FichiersCoursProp',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('description_prop', models.CharField(max_length=100)),
                ('doc_Prop', models.FileField(upload_to='DocumentProp/%Y/%m/%d')),
                ('cours_prop', models.ForeignKey(to='drive.CoursProp')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='Circuit',
        ),
        migrations.DeleteModel(
            name='Image',
        ),
        migrations.DeleteModel(
            name='Javal3',
        ),
        migrations.DeleteModel(
            name='Methodologie',
        ),
        migrations.RemoveField(
            model_name='page',
            name='category',
        ),
        migrations.DeleteModel(
            name='Category',
        ),
        migrations.DeleteModel(
            name='Page',
        ),
        migrations.DeleteModel(
            name='Phpl2',
        ),
        migrations.DeleteModel(
            name='PythonM1',
        ),
    ]
