# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drive', '0003_document_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='document',
            name='docfile1',
            field=models.FileField(upload_to='methodologie/%Y/%m/%d', default=''),
            preserve_default=False,
        ),
    ]
