# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('drive', '0007_document_image'),
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(max_length=200)),
                ('pages', models.IntegerField()),
                ('doc_math', models.FileField(upload_to='maths/%Y/%m/%d')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
