__author__ = 'jb-flo'

from django import forms
from drive.models import Math
from drive.models import FichiersCoursProp
from django.core.urlresolvers import reverse_lazy
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field
from crispy_forms.bootstrap import (
    PrependedText, PrependedAppendedText, FormActions)


class CoursPropForm(forms.ModelForm):
    class Meta:
        model = FichiersCoursProp


# ////////////////////////////////////////////////// old Forms ///////////////////////////////////////////////////////
class DocumentMath(forms.ModelForm):
    class Meta:
        model = Math
        label = 'Select a file',
        help_text = 'max. 42 megabytes'
        success_url = reverse_lazy('main')
        exclude = ('name',)
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('upload', 'upload', css_class='btn-primary'))
