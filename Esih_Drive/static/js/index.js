
function cycleGen (el, opacity, space) {
  return function cycle () {
    sweep(el, ['color'/*, 'border-color'*/], 'hsla(30, 1, .667, ' + opacity + ')', 'hsla(31, 1, .667, ' + opacity + ')', {
      direction: -1,
      duration: 20000,
      callback: cycle,
      space: space
    });
  }
}

cycleGen(document.querySelector('.logo h1'), 0.4, 'HUSL')();
cycleGen(document.querySelector('.logo p'), 0.6, 'HSL')();